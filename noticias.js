const http = require('http')

// Devemos entao criar um servidor
let server = http.createServer( // Este método espera argumentos, que será passado por função
    (req, res) => {

        // Iniciando a chamada pelo tipo de requisição
        let categoria = req.url;

        if (categoria == '/tecnologia') {
            res.end('<html><body>Portal de Tecnologia</body></html>')
         } else if (categoria == '/moda') {
            res.end('<html><body>Portal de Moda</body></html>')
        } else if (categoria == '/beleza') {
            res.end('<html><body>Portal de Beleza</body></html>')
        } else {
            res.end('<html><body>Portal de Notícias</body></html>')
        }

    }   
)

server.listen(3000) // Método .listen indicando a porta 3000 do localhost