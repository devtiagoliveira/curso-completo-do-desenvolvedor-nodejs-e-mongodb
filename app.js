let express = require('express'); // O método espress, retorna uma função, mas ela não é executada
let app = express(); // Aqui chamamos a função

// Informando ao express que a engine de vídeos será o EJS
app.set('view engine', 'ejs')

app.get('/', (req, res) => { // Método get aguarda parâmetros
    res.render('home/index')
})

app.get('/formulario-inclusao-noticia', (req, res) => {
    res.render('admin/form_add_noticia') // com a inclusão do ejs, agora temos o método render
})

app.get('/noticias', (req, res) => {
    res.render('noticias/noticias')
})

app.get('/noticia', (req, res) => {
    res.render('noticias/noticia')
})

app.listen(3000, () => {
    console.log('Servidor ON')
})